﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {
    //
    public GameObject text;
    // Use this for initialization
    public float panSpeed = 20f;
    public float panBorderThickness = 10f;
    float lockPos = 0;
    public Vector2 panLimit;
    public float scrollSpeed = 20f;
    public static List<GameObject> selectionList;
    public Texture compass;
    public GUIStyle MouseDragStyle;
    public static Vector3 currentMouseWorldPos; // World position
    Vector2 mouseDragStart;
    Vector3 mouseDownPoint;
    float timeLimitBeforeDeclareDrag = 1f;
    float timeLeftBeforeDeclareDrag;
    bool isDragging;
    float clickDragZone = 1.3f;
    bool readMM = true;
    Vector3 lastpos = new Vector3();
    Vector3 lastFramePos;
    bool finishedDragging = false;
    public static GameObject selectedBuilding;
    public float hudHeight = Screen.height / 4;
    public static bool Ghosting;

    //////////////////////
    //SelectionBox Calculation Variables
    ///////////////////////
    float boxWidth;
    float boxHeight;
    float boxLeft, boxTop;
    Vector2 boxStart;
    Vector2 boxFinish;
    ////////////////////////////////////////////////////////////////

    public static  List<GameObject> UnitsOnScreen = new List<GameObject>();
    public static List<GameObject> UnitsInDrag = new List<GameObject>();
    bool finishedDragOnThisFrame;

    // Update is called once per frame
    void Start()
    {
        selectionList = new List<GameObject>();
        //FlockScript.flock(3, 4, new Vector3(34, 0, 37));
        hudHeight = Screen.height / 4;
        Debug.Log("HUDHeight = " + hudHeight);
        //lastFramePos = Camera.main.transform.position;
    }
    void Update () {
        

        //Debug.Log("HUDHeight = " + hudHeight);
        // transform.rotation = Quaternion.Euler(lockPos, lockPos, lockPos);
        Vector3 pos = transform.position;

        /////////////////////////////////////////////////////////////
        // Camera Control
        /////////////////////////////////////////////////////////////
        //pos = SideMoveAndZoom(pos);
        pos = onlyKeyboard(pos);
        pos = MMPan(pos);
        //pos = clampPos(pos);
        transform.position = pos;
        /////////////////////////////////////////////////////////////


        bool lmbd = Input.GetMouseButtonDown(0);
        bool lmbu = Input.GetMouseButtonUp(0);
        bool lmb = Input.GetMouseButton(0);
        bool lsb = Input.GetKey(KeyCode.LeftShift);
        bool xb = Input.GetKeyDown(KeyCode.X);
        bool rmb = Input.GetMouseButton(1);

        text.GetComponent<Text>().text = "Mouse : "+Input.mousePosition.y.ToString() + "HUD : " + hudHeight.ToString() ;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray,out hit, Mathf.Infinity))
        {
            if (Input.mousePosition.y > hudHeight)
            {
                currentMouseWorldPos = hit.point;

                if (rmb)
                {
                    foreach (GameObject go in selectionList)
                    {
                        go.GetComponent<UnitController>().MouseInput(1, Input.mousePosition);
                    }
                }

                if (xb)
                {
                    FlockToDestination(currentMouseWorldPos);
                }

                if (lmbu && !isDragging && !lsb && Input.mousePosition.y > hudHeight)
                {
                    SelectSingleUnit();
                }
                else if (lmbu && isDragging)
                {
                    isDragging = false;
                    timeLeftBeforeDeclareDrag = timeLimitBeforeDeclareDrag;
                    finishedDragging = true;
                }
                else if (lmbd)
                {

                    mouseDragStart = Input.mousePosition;
                    mouseDownPoint = hit.point;
                    timeLeftBeforeDeclareDrag = timeLimitBeforeDeclareDrag;
                    isDragging = false;

                }
                else if (lmb)
                {
                    if (!isDragging)
                    {
                        timeLeftBeforeDeclareDrag -= Time.deltaTime;
                        if (timeLeftBeforeDeclareDrag <= 0 || UserDraggingByPostion(mouseDragStart, Input.mousePosition))
                            isDragging = true;
                    }
                    else
                    {
                        //currentMousePos = Input.mousePosition;
                        Debug.Log("Dragging");
                    }
                }
            }

            //text.GetComponent<Text>().text = currentMouseWorldPos.ToString();
            //if(!isDragging)
            //mouseDragStart = currentMousePos;
            

            

        }

        SelectionBoxCalculation();


        if (lmbd && lsb)
            selectUnit();
        
            //SelectSingleUnit();

    }

    void LateUpdate()
    {
        //Debug.Log(string.Format("Number if Units On Screen : {0}",UnitsOnScreen.Count));
        if(finishedDragging)
        {
            for(int i = 0;i < UnitsOnScreen.Count; i++)
            {
                Vector2 screenpos = UnitsOnScreen[i].GetComponent<selectionScript>().screenPos;
                if (UnitInsideDrag(screenpos))
                {
                    UnitsInDrag.Add(UnitsOnScreen[i]);
                    UnitsOnScreen[i].GetComponent<selectionScript>().select();
                }
                else
                {
                   // Debug.Log(string.Format("NOT - Unit Location : {0} - Drag Location : {1} :: {2}",screenpos.ToString(),boxStart.ToString(),boxFinish.ToString()));
                    if (!UnitAlreadyInCurrentSelectedUnits(UnitsOnScreen[i]))
                        UnitsOnScreen[i].GetComponent<selectionScript>().unselect();
                }
            }
            finishedDragging = false;
            PutDraggedUnitsInCurrentlySelectedUnits();
        }
        if (!isDragging)
            UnitsInDrag.Clear();

    }

    static bool ShiftKeysDown()
    {
        if (Input.GetKey(KeyCode.LeftShift))
            return true;
        else
            return false;
    }

//Camera Pan with mouse hitting the sides
    Vector3 SideMoveAndZoom(Vector3 pos)
    {
        
        if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            pos.z += panSpeed * Time.deltaTime;
            Debug.Log("W");
        }
        if (Input.GetKey("s") || Input.mousePosition.y <= panBorderThickness)
        {
            pos.z -= panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            pos.x += panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("a") || Input.mousePosition.x <= panBorderThickness)
        {
            pos.x -= panSpeed * Time.deltaTime;
        }
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y -= scrollSpeed * scroll * Time.deltaTime * 100f;
        return pos;
    }

    //Camera Pan using keyboard only
    Vector3 onlyKeyboard(Vector3 pos)
    {


        if (Input.GetKey("w"))
        {
            pos.z += panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("s"))
        {
            pos.z -= panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("d") )
        {
            pos.x += panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("a"))
        {
            pos.x -= panSpeed * Time.deltaTime;
        }
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y -= scrollSpeed * scroll * Time.deltaTime * 100f;

        return pos;
    }


    //Limit the Axis of camera
    Vector3 clampPos(Vector3 pos)
    {
        pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x);
        pos.z = Mathf.Clamp(pos.z, -panLimit.y, panLimit.y);
        pos.y = Mathf.Clamp(pos.y, 0, 20f);
        return pos;
    }

    Vector3 MMPan(Vector3 pos)
    {

        bool mmbup = Input.GetMouseButtonUp(2);
        bool mmb = Input.GetMouseButton(2);
        if (mmb)
        {
            if (readMM)
            {
                lastpos = Input.mousePosition;
                readMM = false;
            }
            //Cursor.visible = false;
            Vector3 currpos = Input.mousePosition;

          //  Debug.Log(string.Format("Mouse last {0}", lastpos.x));
         //   Debug.Log(string.Format("Mouse Curr {0}", currpos.x));

            if (currpos.x < lastpos.x)
                pos.x -= panSpeed * Time.deltaTime;
            else if (currpos.x > lastpos.x)
                pos.x += panSpeed * Time.deltaTime;
            if (currpos.y < lastpos.y)
                pos.z -= panSpeed * Time.deltaTime;
            else if (currpos.y > lastpos.y)
                pos.z += panSpeed * Time.deltaTime;

        }
        else if (mmbup)
        {
            readMM = true;
          //  Debug.Log("UP");
            // lastpos = Input.mousePosition;
        }

        return pos;
    }

    void FlockToDestination(Vector3 targetDest)
    {
       // Vector3[] targets = FlockScript.flock(selectionList.Count,4,targetDest);
        Vector3[] targets = FlockScript.FormationCenter(selectionList.Count, 5, targetDest);
        //FormationCenter
        for (int i=0;i< selectionList.Count;i++)
        {
            selectionList[i].GetComponent<NavAgentT90>().SetTargetPositionExternally(targets[i]);
        }
    }

    bool UserDraggingByPostion(Vector3 lastPosition, Vector3 currentPosition)
    {
        bool flag = false;

        Vector3 x = lastPosition - currentPosition;
        if (x != Vector3.zero)
            flag = true;

        return flag;
    }
    void SelectionBoxCalculation()
    {
        if (isDragging)
        {
            boxWidth = Camera.main.WorldToScreenPoint(mouseDownPoint).x - Camera.main.WorldToScreenPoint(currentMouseWorldPos).x;
            boxHeight = Camera.main.WorldToScreenPoint(mouseDownPoint).y - Camera.main.WorldToScreenPoint(currentMouseWorldPos).y;
            boxLeft = Input.mousePosition.x;
            boxTop = Screen.height - Input.mousePosition.y - boxHeight;

            if (boxWidth > 0f && boxHeight < 0f)
            {
                boxStart = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }
            else if (boxWidth > 0f && boxHeight > 0f)
            {
                boxStart = new Vector2(Input.mousePosition.x, Input.mousePosition.y + boxHeight);
            }
            else if (boxWidth < 0f && boxHeight < 0f)
            {
                boxStart = new Vector2(Input.mousePosition.x + boxWidth, Input.mousePosition.y);
            }
            else if (boxWidth < 0f && boxHeight > 0f)
            {
                boxStart = new Vector2(Input.mousePosition.x + boxWidth, Input.mousePosition.y + boxHeight);
            }

            boxFinish = new Vector2(boxStart.x + Unsigned(boxWidth), boxStart.y - Unsigned(boxHeight));
        }
       // Debug.Log(string.Format("Start : {0}, Finish : {1}",boxStart.x.ToString(),boxFinish.x.ToString()));
    }

    void SelectionBoxCalculation2()
    {
        if (isDragging)
        {
            boxWidth = Camera.main.WorldToScreenPoint(mouseDownPoint).x - Camera.main.WorldToScreenPoint(currentMouseWorldPos).x;
            boxHeight = Camera.main.WorldToScreenPoint(mouseDownPoint).y - Camera.main.WorldToScreenPoint(currentMouseWorldPos).y;
            boxLeft = Input.mousePosition.x;
            boxTop = Screen.height - Input.mousePosition.y - boxHeight;

            if (boxWidth > 0f && boxHeight < 0f)
            {
                boxStart = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }
            else if (boxWidth > 0f && boxHeight > 0f)
            {
                boxStart = new Vector2(Input.mousePosition.x, Input.mousePosition.y + boxHeight);
            }
            else if (boxWidth < 0f && boxHeight < 0f)
            {
                boxStart = new Vector2(Input.mousePosition.x + boxWidth, Input.mousePosition.y);
            }
            else if (boxWidth < 0f && boxHeight > 0f)
            {
                boxStart = new Vector2(Input.mousePosition.x + boxWidth, Input.mousePosition.y + boxHeight);
            }

            boxFinish = new Vector2(boxStart.x + Unsigned(boxWidth), boxStart.y - Unsigned(boxHeight));
        }
        // Debug.Log(string.Format("Start : {0}, Finish : {1}",boxStart.x.ToString(),boxFinish.x.ToString()));
    }

    float Unsigned(float value)
    {
        if (value < 0f)
            value = value * -1;
        return value;
    }
    void OnGUI()
    {
        if (isDragging)
        {
           // float boxWidth = mouseDragStart.x - Input.mousePosition.x;
           // float boxHeight = mouseDragStart.y - Input.mousePosition.y;
            

            GUI.Box(new Rect(boxLeft, boxTop, boxWidth, boxHeight), "", MouseDragStyle);
        }

        if(!readMM)
        {
            
            float wx = 200;
            float wy = 200;
            float locx = lastpos.x - 100;
            float locy = Screen.height - lastpos.y - 100;

            GUIContent x = new GUIContent("MIDDLE MOUSE", compass);
            GUI.Label(new Rect(locx, locy,wx,wy), x);
        }
    }


    void selectUnit()
    {
            Debug.Log("Mouse and shift");
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
            currentMouseWorldPos = hitInfo.point;

                Debug.Log("Hit " + hitInfo.transform.gameObject.name);
                try {
                    hitInfo.transform.gameObject.GetComponent<selectionScript>().select();
                    selectionList.Add(hitInfo.transform.gameObject);
                    Debug.Log("Called");
                } catch {
                    Debug.Log("Error");
                }
                

               /* if (hitInfo.transform.gameObject.tag == "Construction")
                {
                    Debug.Log("It's working!");
                }
                else
                {
                    Debug.Log("nopz");
                }*/
            }
            else
            {
               // Debug.Log("No hit");
            }
            Debug.Log("Mouse is down");
        
    }

    void SetBuildingWayPoint()
    {
        Vector2 location = Camera.main.WorldToScreenPoint(Input.mousePosition);

    }
    void SelectSingleUnit()
    {
        ClearSelection();
    //    Debug.Log("Mouse is down");
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if (hit)
            if (hitInfo.transform.tag =="Building")
        {
                Debug.Log("Building HIT " + hitInfo.transform.gameObject.name);
                selectedBuilding = hitInfo.transform.gameObject;
                selectedBuilding.GetComponent<BuildingSelection>().Select();
            }
        else
        {
                PlayerHUD.ClearUnits();
            Debug.Log("Hit " + hitInfo.transform.gameObject.name);
            try
            {
                ClearSelection();
                hitInfo.transform.gameObject.GetComponent<selectionScript>().select();
                selectionList.Add(hitInfo.transform.gameObject);
          //      Debug.Log("Called");
            }
            catch
            {
                Debug.Log("Error");
            }


            /* if (hitInfo.transform.gameObject.tag == "Construction")
             {
                 Debug.Log("It's working!");
             }
             else
             {
                 Debug.Log("nopz");
             }*/
        }
        else
        {
            // Debug.Log("No hit");
        }
      //  Debug.Log("Mouse is down");
    }

    static void ClearSelection()
    {
        if(selectedBuilding!=null)
        {
            selectedBuilding.GetComponent<BuildingSelection>().UnSelect();
            selectedBuilding = null;
        }
        for (int i = 0; i < selectionList.Count; i++)
            selectionList[i].GetComponent<selectionScript>().unselect();
        selectionList.Clear();
    }

    public static void RemoveFromOnScreenUnits(GameObject unit)
    {
        int index = UnitsOnScreen.IndexOf(unit);
        UnitsOnScreen.RemoveAt(index);
    }
    public static bool UnitWithinScreenSpace(Vector2 UnitScreenPos)
    {
        if (

            (UnitScreenPos.x<Screen.width && UnitScreenPos.y<Screen.height) &&
            (UnitScreenPos.x >0f && UnitScreenPos.y >0f)
            )
            return true; else return false;
    }
    public bool UnitInsideDrag(Vector2 unitScreenPos)
    {
        if (

            (unitScreenPos.x > boxStart.x && unitScreenPos.y < boxStart.y) &&
            (unitScreenPos.x < boxFinish.x && unitScreenPos.y > boxFinish.y)
            )
            return true;
        else return false;
    }
    static bool UnitAlreadyInCurrentSelectedUnits(GameObject unit)
    {
        return selectionList.Contains(unit);

    }
    public static bool UnitAlreadyInDraggingUnits(GameObject unit)
    {
        return UnitsInDrag.Contains(unit);
    }

    public static void PutDraggedUnitsInCurrentlySelectedUnits()
    {
        if (!ShiftKeysDown())
            ClearSelection();

        if(UnitsInDrag.Count >0)
            for(int i =0; i<UnitsInDrag.Count;i++)
            {
                if (!UnitAlreadyInCurrentSelectedUnits(UnitsInDrag[i]))
                {
                    UnitsInDrag[i].GetComponent<selectionScript>().select();
                    selectionList.Add(UnitsInDrag[i]);
                }
            }
        UnitsInDrag.Clear();
    }
}
