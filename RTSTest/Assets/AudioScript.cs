﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class AudioScript : MonoBehaviour {
    public AudioClip aClip;
    public AudioSource aSource;
    public AudioSource aSource2;
    public List<AudioClip> alClip;
    public List<AudioClip> selectionClips;
    bool playMoveSound = false;
	// Use this for initialization
	void Start () {
        var asources = gameObject.GetComponents<AudioSource>();
     //   Debug.Log(string.Format("Asources count {0}", asources.Length));
        aSource = asources[0];
        aSource2 = asources[1];
        aSource.clip = aClip;
        aSource2.clip= aClip;

    }
	
	// Update is called once per frame
	void Update () {

        bool RMB = Input.GetMouseButtonDown(1);
        bool selected = gameObject.GetComponent<selectionScript>().isSelected();
        if (RMB && selected && aSource.isPlaying ==false)
        {
            int pos = Random.Range(0,alClip.Count);
            aSource.clip = alClip[pos];
            aSource.Play();
        }
        playMoveAudio();


    }

    public void PlaySelectionAudio()
    {
        
    }
    public void StartAudio()
    {
        Debug.Log("StartAudio");
        playMoveSound = true;
       /* while (playMoveSound && aSource.isPlaying == false)
        {
            Debug.Log("LOOP");
            int pos = Random.Range(0, selectionClips.Count);
            aSource.clip = selectionClips[pos];
            aSource.Play();
          //  Thread.Sleep(1000);
            
        }*/
        
    }

    public void playMoveAudio()
    {
        while (playMoveSound && aSource2.isPlaying == false)
        {
            Debug.Log("LOOP");
            int pos = Random.Range(0, selectionClips.Count);
            aSource2.clip = selectionClips[pos];
            aSource2.Play();
            //  Thread.Sleep(1000);

        }
        

    }
    public void StopAudio()
    {
        playMoveSound = false;
        aSource2.Stop();
        Debug.Log("StopAudio");
    }
}
