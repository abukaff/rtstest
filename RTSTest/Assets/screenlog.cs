﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class screenlog : MonoBehaviour {

    static string _message ="";
    static List<string> test;
    static Dictionary<string, string> testdic = new Dictionary<string, string>();
	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        test = new List<string>(testdic.Keys);
    }
    void OnGUI()
    {
        _message = "";
        foreach(string key in test)
        {
            _message += testdic[key] + "\n";
        }
        //GUI.Label(new Rect(10, 10, 200, 200), _message);
        GUI.Label(new Rect(10,10 , 200, Screen.height), _message);
    }
    static public void SetMessage(string keyname, string message)
    {

        if (testdic.ContainsKey(keyname))
            testdic[keyname] = message;
        else
            testdic.Add(keyname, message);
    }
}
