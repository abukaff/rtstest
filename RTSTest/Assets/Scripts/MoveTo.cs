﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTo : MonoBehaviour {

    public float speed = 0.1f;
    public float accel = 0.5f;
    // Use this for initialization
    //   public GameObject humvee;
    //   private Vector3 tragetloc;
    public GameObject graphics;
    Vector3 newposition;
	void Start () {
        // tragetloc = humvee.transform.position;
        newposition = transform.position;
    }
    Ray ray;
    RaycastHit hit;
	// Update is called once per frame
	void Update () {


        function2();


    }

   void function1()
    {
    /*    if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Click registered");

            //Debug.Log(Input.mousePosition);
            Vector3 pos = Input.mousePosition;
            Debug.Log("Mouse 1" + pos);
            pos.z = 20;
            pos = Camera.main.ScreenToWorldPoint(pos);
            pos.y = 0f;
            tragetloc = pos;
            ray = new Ray(pos, Vector3.up);
            Debug.Log("To world" + pos);

            /* if (Physics.Raycast(ray, out hit))
             {
                 //hit.collider.gameObject.

                 hit.collider.GetComponent<Renderer>().material.color = Color.red;
                 Debug.Log(hit);
             }*/
/*
        }

        float step = speed * Time.deltaTime;
        humvee.transform.position = Vector3.MoveTowards(humvee.transform.position, tragetloc, step);
        //humvee.transform.rotation = Vector3.Slerp(humvee.transform.rotation., tragetloc, step);
        //humvee.transform.position = Vector3.RotateTowards(humvee.transform.position, tragetloc, 0, 0);
        Quaternion rot = Quaternion.LookRotation(tragetloc - humvee.transform.position, Vector3.zero);
        rot.x = 0;
        rot.z = 0;
        humvee.transform.rotation = Quaternion.Slerp(rot, humvee.transform.rotation, 0.7f);
        */
    }
    void function2()
    {
        bool RMB = Input.GetMouseButtonDown(1);
        if(RMB)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray,out hit))
            {
                newposition = hit.point;
            }
        }
        Vector3 distance = this.transform.position - newposition;
        //if (!(this.transform.position == newposition))
        if (Vector3.Distance(newposition, this.transform.position) > 2f)
        {
            // newposition.y = 0f;
            Vector2 oldlocation = new Vector2(this.transform.position.x, this.transform.position.z);
            Vector2 newpos = new Vector2(newposition.x,newposition.z);
          //  this.transform.position = Vector2.MoveTowards(oldlocation, newpos, speed * Time.deltaTime);
            this.transform.position = Vector3.MoveTowards(this.transform.position,newposition,speed*Time.deltaTime);
            Quaternion transrot = Quaternion.LookRotation(newposition - this.transform.position, Vector3.up);
            transrot.z = 0;
            transrot.x = 0;
            graphics.transform.rotation = Quaternion.Slerp(transrot,graphics.transform.rotation,0.7f);
            Debug.Log(transrot.ToString());

        }
    }

    Vector3 LookAT;
    Quaternion playerRot;
    void function3()
    {
        bool RMB = Input.GetMouseButtonDown(1);
        if (RMB)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                newposition = hit.point;
                LookAT = new Vector3(newposition.x-this.transform.position.x,this.transform.position.y,newposition.z-this.transform.position.z);
                playerRot = Quaternion.LookRotation(LookAT);
            }
        }

    }
}
