﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavAgent : MonoBehaviour {

 //   NavAgent agent;
    NavMeshAgent agent;
    Vector3 target;
    float Distance;
    public GameObject bone;
    Quaternion AimAT;
    // Use this for initialization
    void Start () {
        target = transform.position;
        agent = GetComponent<NavMeshAgent>();
      
    }
    
    // Update is called once per frame
    void Update () {
        bool RMB = Input.GetMouseButtonDown(1);
        bool isSelected = gameObject.GetComponent<selectionScript>().isSelected();

        if (RMB && isSelected == true)
        {
            Debug.Log("Right CLicked");
            SetTargetPosition();
            
         
          //  gameObject.GetComponent<AudioScript>().StartAudio();
        }

        Distance = Vector3.Distance(target, transform.position);
        if(Distance > 0.5f)
        agent.SetDestination(target);
        //  AIM(AimAT);

        if (Distance < 0.5f)
        {
            agent.isStopped = true;
            //target = transform.position;
        }
        
        if(agent.isStopped == true)
        {
          //  Debug.Log("Nav Stopped");
           
         //   gameObject.GetComponent<AudioScript>().StopAudio();
        }


        NavMeshPath path = new NavMeshPath();
        GetComponent<NavMeshAgent>().CalculatePath(target, path);
        if (path.status == NavMeshPathStatus.PathPartial)
        {
            agent.isStopped = true;
            //reset a path
        }

        try
        {
            if (agent.isStopped == false)
            {
                GetComponent<AnimationControlScript>().moving();
                GetComponent<AnimationControlScript>().isAiming(false);
                //Debug.Log("Moving : Distance : " + Distance.ToString());
            }
            else
            {
                GetComponent<AnimationControlScript>().stopped();

            }
        }
        catch { }



    }
    void AIM(Quaternion rotation)
    {
        bone.transform.rotation = Quaternion.Slerp(bone.transform.rotation, AimAT, 1 * Time.deltaTime);
        float diff = bone.transform.rotation.eulerAngles.y - AimAT.eulerAngles.y; float dergee = 5; if (Mathf.Abs(diff) <= dergee) { Debug.Log("ready"); }
    }

    void SetTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000))
        {
            if (hit.transform.name == "Terrain" || hit.transform.name == "Cube")
            {
                target = hit.point;
                agent.isStopped = false;
            }
            else
            {
                Vector3 Aimtarget = hit.point;
                Vector3 lookAtTarget = new Vector3(
                     Aimtarget.x - this.transform.position.x,
                     this.transform.position.y,
                     Aimtarget.z -this.transform.position.z);

                AimAT = Quaternion.LookRotation(-lookAtTarget);
                //AimAT.x = 0;
              //  AimAT.z = 0;


            }

        }
    }

}
