﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCamera : MonoBehaviour {

    // Use this for initialization
    //public List<GameObject> Cameralist;
    public Camera cam1;
    public Camera cam2;
    
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        bool zkey = Input.GetKeyDown(KeyCode.Z);
        if(zkey)
        {
            if(cam1.enabled ==true)
            {
                cam2.enabled = true;
                cam1.enabled = false;
                
            }
            else
            {
                cam1.enabled = true;
                cam2.enabled=false;
                
            }
        }
	}
}
