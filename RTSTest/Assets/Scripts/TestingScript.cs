﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingScript : MonoBehaviour {

    public GameObject ghosting;
    public GameObject actualBuilding;
    GameObject ghost;
    bool isGhosting = false;
    bool locker = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKeyDown(KeyCode.R))
        {
            isGhosting = true;
        }
        if(isGhosting)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Vector3 location = hit.point;
                if (locker ==false)
                {
                    ghost= Instantiate(ghosting, location, Quaternion.LookRotation(Vector3.forward));
                    locker = true;
                }
                ghost.transform.position = location;
            }

        }
        if(Input.GetMouseButtonDown(1))
        {
            if(isGhosting)
            {
                Destroy(ghost);
                locker = false;
                isGhosting = false;
            }
        }
        if(Input.GetMouseButtonDown(0))
        {
            if (isGhosting && ghost.GetComponent<BuildingGhosting>().isAllowed == true)
            {
                Vector3 location = ghost.transform.position;
                Destroy(ghost);
                locker = false;
                isGhosting = false;
                Instantiate(actualBuilding, location, Quaternion.LookRotation(Vector3.forward));
            }
        }
	}
}
