﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildUnitClass {

    // Use this for initialization
    public Texture2D unitsIconTexture;
    public Texture2D unitsIconTextureRO;
    public string unitsName;
    public string unitPath;

}
