﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class selectionScript : MonoBehaviour {
    bool selected = false;
    bool onScreen = false;
    public GameObject selectionIndecator;
    public Vector2 screenPos;
    
    /*// Use this for initialization
	void Start () {
		
	}
	*/
	// Update is called once per frame
	void Update () {
        screenPos = Camera.main.WorldToScreenPoint(this.transform.position);
        if(CameraController.UnitWithinScreenSpace(screenPos))
        {
            if (!onScreen)
            {
                CameraController.UnitsOnScreen.Add(this.gameObject);
                onScreen = true;
            }
        }
        else
        {
            if(onScreen)
            {
                CameraController.RemoveFromOnScreenUnits(this.gameObject);
                onScreen = false;

            }
        }

    }

    public void select()
    {
        selected = true;
        selectionIndecator.GetComponent<Renderer>().enabled = true;
        try
            {
           // if (isDozer)
           if(gameObject.GetComponent<UnitData>().unitType == EnumsClass.UnitType.Dozer)
                gameObject.GetComponent<DozerBuildGUIScript>().ShowGUI();
        }
        catch
        {
        }
        

    }
    public void unselect()
    {
        selected = false;
        selectionIndecator.GetComponent<Renderer>().enabled = false;
    }

    public bool isSelected()
    {
        return selected;
    }
}
