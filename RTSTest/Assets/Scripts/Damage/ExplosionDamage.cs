﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDamage : MonoBehaviour {
    public float damageClose =50 ;
    public float damageFar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider collision)
    {
        try
        {
            Debug.Log("Damaging : " + collision.gameObject.name);
            collision.gameObject.GetComponent<UnitStats>().TakePhysicalDamage(damageClose);
        }
        catch
        {
            Debug.LogError("ExplosionDamage : " + collision.gameObject.name);
        }
    }

}
