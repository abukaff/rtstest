﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostingBuildingScript : MonoBehaviour {
    public GameObject actualBuilding;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

            Vector3 location = CameraController.currentMouseWorldPos;
            gameObject.transform.position = location;

    }
}
