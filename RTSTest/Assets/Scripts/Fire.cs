﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour {

    selectionScript selection;
    public ParticleSystem MuzzleFlash;
    public GameObject impactEffect;
    public GameObject turret;
    public Animator x;
    public GameObject shell;
    public GameObject projectileStartLocation;
    public float fireRate = .5f;
    private float nextTimeToShoot = 0f;
    int jumpHash;
    // Use this for initialization
    void Start () {

        selection = gameObject.GetComponent<selectionScript>();
        //x = gameObject.GetComponent<Animator>();
        //jumpHash = Animator.StringToHash("Shoot");

    }
	
	// Update is called once per frame
	void Update () {

        Shoot();
        

    }

    
    void Shoot()
    {
        //bool spacePressed = Input.GetKeyDown(KeyCode.Space);
        bool spacePressed = Input.GetKey(KeyCode.Space);
        if (selection.isSelected() && spacePressed && Time.time>=nextTimeToShoot)
        {
            nextTimeToShoot = Time.time + 1f / fireRate;
            //MuzzleFlash.Play();
            // x.SetTrigger(jumpHash);
            //Vector3 yup = turret.transform.position + new Vector3(0, 0, 0);
            GameObject ImpactGO = Instantiate(shell, projectileStartLocation.transform.position, projectileStartLocation.transform.rotation);
            //GameObject ImpactGO = Instantiate(shell, projectileStartLocation.transform.position, turret.transform.rotation);
            ImpactGO.GetComponent<Rigidbody>().AddForce(projectileStartLocation.transform.forward * 2000);
            //ImpactGO.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 2000);
            Destroy(ImpactGO, 6f);

            GetComponent<AnimationControlScript>().isAiming(true);
            GetComponent<AnimationControlScript>().Shoot();

            //x.Play("T90");
            /* RaycastHit hit;
             if (Physics.Raycast(gameObject.transform.position,  turret.transform.forward,out hit))
             {
                 Debug.Log(hit.transform.name);
                 Vector3 yup = gameObject.transform.position;
                 GameObject ImpactGO =  Instantiate(shell, yup, Quaternion.LookRotation(hit.normal));
                 ImpactGO.GetComponent<Rigidbody>().AddForce(gameObject.transform.forward * 100);
                 Destroy(ImpactGO, 3f);
             }*/

        }


        
    }

    void Shootbackup()
    {
        bool spacePressed = Input.GetKeyDown(KeyCode.Space);
        if (selection.isSelected() && spacePressed)
        {
            MuzzleFlash.Play();
            x.SetTrigger(jumpHash);
            //x.Play("T90");
            RaycastHit hit;
            if (Physics.Raycast(gameObject.transform.position, turret.transform.forward, out hit))
            {
                Debug.Log(hit.transform.name);
                Vector3 yup = hit.point + new Vector3(0, 2, 0);
                GameObject ImpactGO = Instantiate(impactEffect, yup, Quaternion.LookRotation(hit.normal));
                Destroy(ImpactGO, 3f);
            }
        }
    }
}
