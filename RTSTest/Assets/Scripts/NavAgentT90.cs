﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavAgentT90 : MonoBehaviour {


    //   NavAgent agent;
    NavMeshAgent agent;
    Vector3 targetLocation;
    float Distance;
    public GameObject duster;
    ParticleSystem ps;

    public GameObject turret;
    
    public Vector3 waypoint;
    bool isSelected = false;

    bool newSpawn = true;
    Vector3 ZeroVector = new Vector3(0,0,0);
    public GameObject rangeObject;


    // Aiming Variables
    float timeToRotateBack = 2;
    public float aimRotationSpeed = 1.5f;
    Quaternion turrerZeroRotation;
    bool isAiming = false;
    bool isForcedAiming = false;
    Vector3 _aimTarget;
    GameObject _aimObject;
    Quaternion AimAT;
    GameObject _forcedAim;
    
    // Use this for initialization
    void Start()
    {
        targetLocation = transform.position;
        agent = GetComponent<NavMeshAgent>();
        ps = duster.GetComponent<ParticleSystem>();
        Debug.Log("from T90 Waypoint = " + waypoint);
        if(turret != null)
        turrerZeroRotation = turret.transform.rotation;

        //agent.SetDestination(waypoint);
    }
    
    // Update is called once per frame
    void Update()
    {
        if (newSpawn && waypoint != ZeroVector)
            targetLocation = waypoint;

        bool RMB = Input.GetMouseButtonDown(1);
        isSelected = gameObject.GetComponent<selectionScript>().isSelected();

        

        Distance = Vector3.Distance(targetLocation, transform.position);
        agent.SetDestination(targetLocation);
        if (isAiming && _aimObject != null && _forcedAim == null)
        {
            timeToRotateBack = 2;
            // AIMAT(_aimTarget);
            AIMAT(_aimObject.transform.position);
            AIM(AimAT);

        }
        else if(isForcedAiming && _forcedAim != null)
        {
            float tempDistance = Vector3.Distance(_forcedAim.transform.position, transform.position);
            float range = rangeObject.GetComponent<SphereCollider>().radius;
            ForcedAIMAT(_forcedAim.transform.position);
            AIM(AimAT);
            if (tempDistance > range)
            {
                Debug.Log(string.Format("Distance : {0}, Range : {1}",tempDistance,range));
                targetLocation = _forcedAim.transform.position;
            }
            else
                agent.isStopped = true;
        }
        else
        {
            timeToRotateBack -= Time.deltaTime;
            if (timeToRotateBack < 0)
            {
                turrerZeroRotation = Quaternion.LookRotation(gameObject.transform.forward);
                turret.transform.rotation = Quaternion.Slerp(turret.transform.rotation, turrerZeroRotation, aimRotationSpeed * Time.deltaTime);
            }
        }
        

        if (Distance < 0.5f)
            agent.isStopped = true;
        if (agent.isStopped == true)
        {
          //  Debug.Log("Nav Stopped");
            ps.Stop();
            //   gameObject.GetComponent<AudioScript>().StopAudio();
        }


        NavMeshPath path = new NavMeshPath();
        GetComponent<NavMeshAgent>().CalculatePath(targetLocation, path);
        if (path.status == NavMeshPathStatus.PathPartial)
        {
           // agent.isStopped = true;
            //reset a path
        }
        OnDrawGizmosSelected();

    }

    public void RightClicked(Vector3 destinationLocation)
    {
        if (isSelected == true)
        {
            Debug.Log("Right CLicked");
            SetTargetPositionExternally(destinationLocation);
            //agent.isStopped = false;

            //  gameObject.GetComponent<AudioScript>().StartAudio();
        }
    }

    public void SetTargetPosition(Vector3 mouseLocation)
    {
        newSpawn = false;
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //Debug.Log(string.Format("Mouse : {0}, Destination Location: {1}",Input.mousePosition, mouseLocation));
        Ray ray = Camera.main.ScreenPointToRay(mouseLocation);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000))
        {

            if (hit.transform.name == "Terrain")
            {
                targetLocation = hit.point;
                agent.isStopped = false;
                if (ps.isPlaying == false)
                    ps.Play();
                _forcedAim = null;
                isForcedAiming = false;
            }
            else
            {

                _forcedAim = hit.transform.gameObject;
                _aimTarget = hit.point;
                //_aimObject = hit.transform.gameObject;
                isAiming = true;
                /*Vector3 Aimtarget = hit.point;
                Aimtarget = hit.point;
                AIMAT(Aimtarget);*/
                //AimAT.x = 0;
                //  AimAT.z = 0;
                isForcedAiming = true;
                agent.isStopped = false;


            }

        }
    }
    public void SetTargetPositionExternally(Vector3 destinationLocation)
    {
        targetLocation = destinationLocation;
        agent.isStopped = false;
        if (ps.isPlaying == false)
            ps.Play();
    }
    public void SetTargetPositionExternallyMouseCords(Vector3 MousePosition)
    {
        newSpawn = false;
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //Debug.Log(string.Format("Mouse : {0}, Destination Location: {1}",Input.mousePosition, mouseLocation));
        Ray ray = Camera.main.ScreenPointToRay(MousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000))
        {

            if (hit.transform.name == "Terrain")
            {
                targetLocation = hit.point;
                agent.isStopped = false;
                if (ps.isPlaying == false)
                    ps.Play();
            }
            else
            {
                _aimTarget = hit.point;
                _aimObject = hit.transform.gameObject;
                isAiming = true;
                /*Vector3 Aimtarget = hit.point;
                Aimtarget = hit.point;
                AIMAT(Aimtarget);*/
                //AimAT.x = 0;
                //  AimAT.z = 0;

            }

        }
    }
    void AIMAT(Vector3 aimTarget)
    {
        float distance = Vector3.Distance(gameObject.transform.position,aimTarget);
        float _range = rangeObject.GetComponent<SphereCollider>().radius;
        if (distance < _range)
        {
            Vector3 lookAtTarget = new Vector3(
                 aimTarget.x - this.transform.position.x,
                 this.transform.position.y,
                 aimTarget.z - this.transform.position.z);

            AimAT = Quaternion.LookRotation(lookAtTarget);
        }
        else
            isAiming = false;
    }

    void ForcedAIMAT(Vector3 aimTarget)
    {
        
            Vector3 lookAtTarget = new Vector3(
                 aimTarget.x - this.transform.position.x,
                 this.transform.position.y,
                 aimTarget.z - this.transform.position.z);

            AimAT = Quaternion.LookRotation(lookAtTarget);
        isForcedAiming = true;

    }

    public void AimAtTarget(GameObject targetObject)
    {
        if(isAiming==false)
        {
            _aimObject = targetObject;
            isAiming = true;
        }
        
    }

    void AIM(Quaternion rotation)
    {
        turret.transform.rotation = Quaternion.Slerp(turret.transform.rotation, AimAT, aimRotationSpeed * Time.deltaTime);
        float diff = turret.transform.rotation.eulerAngles.y - AimAT.eulerAngles.y;
        float dergee = 2f;
        if (Mathf.Abs(diff) <= dergee) {
        //    Debug.Log("ready");
        }
    }



    void OnDrawGizmosSelected()
    {

        var nav = GetComponent<NavMeshAgent>();
        if (nav == null || nav.path == null)
            return;

        var line = this.GetComponent<LineRenderer>();
        if (line == null)
        {
            line = this.gameObject.AddComponent<LineRenderer>();
            line.material = new Material(Shader.Find("Sprites/Default")) { color = Color.yellow };
            line.SetWidth(0.5f, 0.5f);
            //line.SetColors(Color.yellow, Color.yellow);
            line.startColor = Color.yellow;
            line.endColor = Color.green;
        }

        var path = nav.path;

        //line.SetVertexCount(path.corners.Length);
        line.positionCount = path.corners.Length;

        for (int i = 0; i < path.corners.Length; i++)
        {
            line.SetPosition(i, path.corners[i]);
        }

    }


}
