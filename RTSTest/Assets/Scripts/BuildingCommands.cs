﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCommands : MonoBehaviour {   
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BuildUnit(string path)
    {
        GameObject obj = Resources.Load(path) as GameObject;
        GameObject waypoint = gameObject.GetComponent<BuildingSelection>().waypoint;
        obj.GetComponent<NavAgentT90>().waypoint = waypoint.transform.position;
        Vector3 spwanlocation = gameObject.transform.position + new Vector3(0,0,10);
        Instantiate(obj, spwanlocation , Quaternion.LookRotation(Vector3.forward));
        //Debug.Log("from BuildCommand Waypoint = " + waypoint.transform.position);
    }
    public void BuildBuilding(string path)
    {
        Debug.Log("BuildBuilding");
        GameObject obj = Resources.Load(path) as GameObject;
        Instantiate(obj, CameraController.currentMouseWorldPos, Quaternion.LookRotation(Vector3.forward));
        // GameObject waypoint = gameObject.GetComponent<BuildingSelection>().waypoint;
        //obj.GetComponent<NavAgentT90>().waypoint = waypoint.transform.position;
        // Vector3 spwanlocation = gameObject.transform.position + new Vector3(0, 0, 10);
        // Instantiate(obj, spwanlocation, Quaternion.LookRotation(Vector3.forward));
        //Debug.Log("from BuildCommand Waypoint = " + waypoint.transform.position);
    }
}
