﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingSelection : MonoBehaviour {

    bool selected = false;
    LineRenderer line;
    //Inputs
    bool rmbu;
    public GameObject waypoint;
    public GameObject selectionIndecator;
    public Canvas canvas;
    
    // Use this for initialization
    void Start () {

        //line= this.gameObject.GetComponent<LineRenderer>();
        //Button b = gameObject.GetComponent<Button>();
        //b.onClick.AddListener(delegate () { StartGame("Level1"); });
    }

    public void StartGame(string level)
    {
        Debug.Log(level);
        //Application.LoadLevel(level);
    }

    // Update is called once per frame
    void Update () {
       // rmbu = Input.GetMouseButtonUp(1);
        if (selected)
        {
            rmbu = Input.GetMouseButtonUp(1);
            
         //   line.enabled = true;
            if (rmbu)
            {
                waypoint.transform.position = CameraController.currentMouseWorldPos;
                Debug.Log(string.Format("Setting Waypoint {0}", waypoint.transform.position.ToString()));
                DrawLine();

                /*Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                 RaycastHit hit;
                 if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                 {
                     waypoint.transform.position = hit.point;
                     Debug.Log(string.Format("Setting Waypoint {0}", waypoint.transform.position.ToString()));
                     DrawLine();
                 }*/


                //  DrawLine();
            }
        }
        else
        { }
        //    line.enabled = false;
	}

    public bool isSelected()
    {
        return selected;
    }
    public void Select()
    {
        selected = true;
        selectionIndecator.GetComponent<MeshRenderer>().enabled = true;
        waypoint.GetComponent<MeshRenderer>().enabled = true;
        //canvas.GetComponent<Canvas>().enabled = true;
        gameObject.GetComponent<Warfactory_russia>().ShowGUI();
        if (line!=null)
        line.enabled = true;
    }
    public void UnSelect()
    {
        selected = false;
        selectionIndecator.GetComponent<MeshRenderer>().enabled = false;
        waypoint.GetComponent<MeshRenderer>().enabled = false;
       // canvas.GetComponent<Canvas>().enabled = false;
        if (line != null)
            line.enabled = false;
    }
    void DrawLine()
    {
        line = this.GetComponent<LineRenderer>();
        if (line == null)
        {
            line = this.gameObject.AddComponent<LineRenderer>();
            line.material = new Material(Shader.Find("Sprites/Default")) { color = Color.yellow };
            line.SetWidth(0.5f, 0.5f);
            line.SetColors(Color.yellow, Color.yellow);
        }
        line.SetPosition(0, this.transform.position);
        line.SetPosition(1, waypoint.transform.position);
    }
}
