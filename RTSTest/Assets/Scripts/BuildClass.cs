﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class is used for reading the prefabs from Resources folder and populat them in the GUI
public class BuildClass {

    // Use this for initialization
    public Texture2D objectIconTexture;
    public Texture2D objectIconTextureRO;
    public string objectName;
    public string objectPath;
    
}
