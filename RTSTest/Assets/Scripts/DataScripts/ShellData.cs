﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellData : MonoBehaviour {
    public float shellSpeed;
    public GameObject impactEffect;
    public GameObject owner;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider collision)
    {
        try
        {
            
            Vector3 yup = gameObject.transform.position;
            GameObject ImpactGO = Instantiate(impactEffect, yup, Quaternion.LookRotation(yup));
            Destroy(ImpactGO, 3f);
            //Debug.Log("ShellData" + collision.gameObject.name);
        }
        catch
        {
            Debug.LogError("ShellData" + collision.gameObject.name);
        }
        Destroy(this.gameObject);
    }
}
