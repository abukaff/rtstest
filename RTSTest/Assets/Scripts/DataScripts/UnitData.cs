﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitData : MonoBehaviour{

    public string unitName;
    public Texture2D unitImage;
    public Texture2D unitImageRO;
    public EnumsClass.UnitType unitType;
    // Use this for initialization

}
