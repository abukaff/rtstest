﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStats : MonoBehaviour {
    public float currentHP = 100;
    public float maxHP = 100;
    public float phisycalArmourPercentage = 0;
    public float radiationArmourPercentage = 0;
    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(currentHP <= 0)
        {
            Destroy(gameObject);
        }
	}

    public void TakePhysicalDamage(float damageAmount)
    {
        float reduction = damageAmount * (phisycalArmourPercentage / 100);
        damageAmount = damageAmount - reduction;
        currentHP = currentHP - damageAmount;
    }
    public void TakeRadiationDamage(float damageAmount)
    {
        float reduction = damageAmount * (radiationArmourPercentage / 100);
        damageAmount = damageAmount - reduction;
        currentHP = currentHP - damageAmount;
    }
}
