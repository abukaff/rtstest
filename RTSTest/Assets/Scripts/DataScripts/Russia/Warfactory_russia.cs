﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warfactory_russia : MonoBehaviour {

    List<Texture2D> _unitsIconTexture;
    List<Texture2D> _unitsIconTextureRO;
    List<string> _unitsNames;
    List<string> _unitPath;
    List<BuildUnitClass> UnitsList;
    //List<List<string>> test;
    // Use this for initialization
    void Start () {

        _unitsIconTexture = new List<Texture2D>();
        _unitsIconTextureRO = new List<Texture2D>();
        _unitsNames = new List<string>();
        _unitPath = new List<string>();
        UnitsList = new List<BuildUnitClass>();

        //string path = "3D/Prefabs/Latest/Russia/Warfactory";
        string path = "Prefabs/Russia/Warfactory";
        Object[] Units = Resources.LoadAll(path);
        if (Units.Length > 0)
        {
            Debug.Log(string.Format("Number of Units : {0}", Units.Length));
            for (int i = 0; i < Units.Length; i++)
            {
                GameObject obj = Units[i] as GameObject;
                Texture2D uniticon = obj.GetComponent<UnitData>().unitImage;
                Texture2D uniticonRO = obj.GetComponent<UnitData>().unitImageRO;
                string name = obj.GetComponent<UnitData>().unitName;
                string unitpath = path + "/" + obj.name;

               /* _unitsIconTexture.Add(uniticon);
                _unitsIconTextureRO.Add(uniticonRO);
                _unitsNames.Add(name);
                _unitPath.Add(unitpath);*/

                BuildUnitClass unitObject = new BuildUnitClass();
                unitObject.unitsIconTexture = obj.GetComponent<UnitData>().unitImage;
                unitObject.unitsIconTextureRO = obj.GetComponent<UnitData>().unitImageRO;
                unitObject.unitsName = obj.GetComponent<UnitData>().unitName;
                unitObject.unitPath = path + "/" + obj.name;
                UnitsList.Add(unitObject);
                // unitsIconTexture.Add()
            }
           // PlayerHUD.AddToView(unitsIconTexture, unitsIconTextureRO, unitsNames, unitPath);
            
        }
        else
            Debug.Log("NO UNITS");

    }
	
	// Update is called once per frame
	void Update () {
      //  Debug.Log("GUI Number of Units : " + _unitsNames.Count);
    }

    public void ShowGUI()
    {

        //Debug.Log("GUI Number of Units : " + unitsNames.Count);
        //PlayerHUD.AddToView(_unitsIconTexture, _unitsIconTextureRO, _unitsNames, _unitPath);
        PlayerHUD.AddToView(UnitsList);
    }
    public void Build(string path)
    {
        Debug.Log(string.Format("Path : {0}", path));
        GameObject obj = Resources.Load(path) as GameObject;
        GameObject waypoint = gameObject.GetComponent<BuildingSelection>().waypoint;
        Instantiate(obj, waypoint.transform.position, Quaternion.LookRotation(Vector3.forward));
    }


}
