﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DozerBuildGUIScript : MonoBehaviour {
    public string path = "Prefabs/Russia/Buildings/Ghosting";

    List<BuildClass> objectsList;
    // Use this for initialization
    void Start () {
        Object[] Units = Resources.LoadAll(path);
        objectsList = new List<BuildClass>();
        if (Units.Length > 0)
        {
            //Debug.Log(string.Format("Number of Units : {0}", Units.Length));
            for (int i = 0; i < Units.Length; i++)
            {
                GameObject obj = Units[i] as GameObject;
               /* Texture2D uniticon = obj.GetComponent<BuildClass>().objectIconTexture;
                Texture2D uniticonRO = obj.GetComponent<BuildClass>().objectIconTextureRO;
                string name = obj.GetComponent<BuildClass>().objectName;
                string unitpath = path + "/" + obj.name;*/

                /* _unitsIconTexture.Add(uniticon);
                 _unitsIconTextureRO.Add(uniticonRO);
                 _unitsNames.Add(name);
                 _unitPath.Add(unitpath);*/

                BuildClass unitObject = new BuildClass();
                unitObject.objectIconTexture = obj.GetComponent<UnitData>().unitImage;
                unitObject.objectIconTextureRO = obj.GetComponent<UnitData>().unitImageRO;
                unitObject.objectName = obj.GetComponent<UnitData>().unitName;
                unitObject.objectPath = path + "/" + obj.name;
                objectsList.Add(unitObject);
                // unitsIconTexture.Add()
            }
            // PlayerHUD.AddToView(unitsIconTexture, unitsIconTextureRO, unitsNames, unitPath);

        }


    }

    public void ShowGUI()
    {

        //Debug.Log("GUI Number of Units : " + unitsNames.Count);
        //PlayerHUD.AddToView(_unitsIconTexture, _unitsIconTextureRO, _unitsNames, _unitPath);
        PlayerHUD.AddToView(objectsList);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
