﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitsNavigation : MonoBehaviour {
    NavMeshAgent agent;
    Vector3 targetLocation;
    float Distance;
    public Vector3 waypoint;
    bool isSelected = false;
    bool newSpawn = true;
    Vector3 ZeroVector = new Vector3(0, 0, 0);
    public float stopingDistance = .5f;

    // Use this for initialization
    void Start () {
        targetLocation = transform.position;
        agent = GetComponent<NavMeshAgent>();


        if (newSpawn && waypoint != ZeroVector)
            targetLocation = waypoint;

    }
	
	// Update is called once per frame
	void Update () {
        isSelected = gameObject.GetComponent<selectionScript>().isSelected(); // check if unit is selected
        Distance = Vector3.Distance(targetLocation, transform.position); // Calculates the distance remaining for stop functionality
        OnDrawGizmosSelected(); // Draws line of the path
        DestinationReached(); // Stops When Distination is reached
        if (Distance > stopingDistance)
            agent.SetDestination(targetLocation); // Sets the location so it can move
        screenlog.SetMessage("UnitNav", "UnitNav : IsStopped : " + agent.isStopped.ToString());
        screenlog.SetMessage("UnitNavDestance", "UnitNav : Distance : " + Distance.ToString());
    }

    public void Stop()
    {
        agent.isStopped = true;
    }
    public void SetDestination(Vector3 Destination)
    {
            targetLocation = Destination;
       // if(Distance>stopingDistance)
            agent.isStopped = false;
    }
    public void SetDestination(GameObject Destination)
    {
        targetLocation = Destination.transform.position;
        // if(Distance>stopingDistance)
        agent.isStopped = false;
    }
    bool DestinationReached()
    {
        if(Distance<= stopingDistance)
        {
            agent.isStopped = true;
            return true;
        }
        else
        return false;
    }
    public bool isStopped()
    {
        return agent.isStopped;
    }

    void OnDrawGizmosSelected()
    {

        var nav = GetComponent<NavMeshAgent>();
        if (nav == null || nav.path == null)
            return;

        var line = this.GetComponent<LineRenderer>();
        if (line == null)
        {
            line = this.gameObject.AddComponent<LineRenderer>();
            line.material = new Material(Shader.Find("Sprites/Default")) { color = Color.yellow };
            line.SetWidth(0.5f, 0.5f);
            //line.SetColors(Color.yellow, Color.yellow);
            line.startColor = Color.yellow;
            line.endColor = Color.green;
        }

        var path = nav.path;

        //line.SetVertexCount(path.corners.Length);
        line.positionCount = path.corners.Length;

        for (int i = 0; i < path.corners.Length; i++)
        {
            line.SetPosition(i, path.corners[i]);
        }

    }
}
