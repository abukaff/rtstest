﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FlockScript : MonoBehaviour {

    static NavMeshAgent xx;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static Vector3[] flock(int numberOfUnits,float unitRadius, Vector3 TargetLocation)
    {
        Debug.Log("FLOCK - Units : " + numberOfUnits.ToString());
        Vector3[] calculatedLocations = new Vector3[numberOfUnits];
        List<Vector3> calcTemp = new List<Vector3>();
        float xMax = TargetLocation.x + (numberOfUnits * unitRadius);
        float xMin = TargetLocation.x - (numberOfUnits * unitRadius);
        float zMax = TargetLocation.z + (numberOfUnits * unitRadius);
        float zMin = TargetLocation.z - (numberOfUnits * unitRadius);
        string debugLocations = "";
        for (int i = 0; calcTemp.Count < numberOfUnits; i++)
        {
            float xpos = Random.Range(xMin, xMax);
            float zpos = Random.Range(zMin, zMax);
            Vector3 tempTargetLoc = new Vector3(xpos, TargetLocation.y, zpos);
            
            bool xallowed = true;
            bool zallowed = true;
            for (int r = 0; r < calcTemp.Count;r++)
            {
                if(Unsigned(calcTemp[r].x - xpos) < unitRadius)
                {
                    xallowed = false;
                }
                if(Unsigned(calcTemp[r].z - zpos) < unitRadius)
                {
                    zallowed = false;
                }
            }
            if (xallowed || zallowed)
                calcTemp.Add(tempTargetLoc);
        }
        
        for(int q=0;q< numberOfUnits;q++)
        {
            calculatedLocations[q] = calcTemp[q];
            debugLocations += string.Format("Location {0} : {1}\n", q, calculatedLocations[q]);
        }
       
        Debug.Log(debugLocations);
        return calculatedLocations;
    }

    static float Unsigned(float value)
    {
        if (value < 0f)
            value = value * -1;
        return value;
    }


    public static Vector3[] FormationCenter(int numberOfUnits, float unitRadius, Vector3 TargetLocation)
    {
        Debug.Log("Formation - Units : " + numberOfUnits.ToString());
        Vector3[] calculatedLocations = new Vector3[numberOfUnits];
        List<Vector3> calcTemp = new List<Vector3>();
        float xMax = TargetLocation.x + (numberOfUnits * unitRadius);
        float xMin = TargetLocation.x - (numberOfUnits * unitRadius);
        float zMax = TargetLocation.z + (numberOfUnits * unitRadius);
        float zMin = TargetLocation.z - (numberOfUnits * unitRadius);
        string debugLocations = "";
        for (int i = 0; calcTemp.Count < numberOfUnits; i++)
        {
            float xpos = TargetLocation.x;
            float ypos = TargetLocation.y;
            float zpos = TargetLocation.z;
            if ((i%2==0))
                zpos = TargetLocation.z + (i * unitRadius);
            else
                zpos = TargetLocation.z - (i * unitRadius);

            Vector3 tempTargetLoc = new Vector3(xpos, ypos, zpos);
            calcTemp.Add(tempTargetLoc);
        }

        for (int q = 0; q < numberOfUnits; q++)
        {
            calculatedLocations[q] = calcTemp[q];
            debugLocations += string.Format("Location {0} : {1}\n", q, calculatedLocations[q]);
        }
        Debug.Log(debugLocations);
        return calculatedLocations;
    }
}
