﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGhosting : MonoBehaviour {
    public GameObject ghostingObject;
    public Material green;
    public Material Red;
    bool allowed =true;
    public bool isAllowed //
    {
        get { return allowed; }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (allowed)
            ghostingObject.GetComponent<MeshRenderer>().material = green;
        else
            ghostingObject.GetComponent<MeshRenderer>().material = Red;

        Vector3 location = CameraController.currentMouseWorldPos;
        gameObject.transform.position = location;
    }
    public void Allowed()
    {
        allowed = true;
    }
    public void NotAllowed()
    {
        allowed = false;
    }

    void OnTriggerStay(Collider collision)
    {
        NotAllowed();
    }
    void OnTriggerExit(Collider collision)
    {
        Allowed();
    }

}
