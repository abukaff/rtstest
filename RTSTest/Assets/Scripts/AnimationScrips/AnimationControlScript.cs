﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControlScript : MonoBehaviour {
    Animator anim;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void moving()
    {
        anim.SetBool("isMoving", true);
       // Debug.Log("isMoving");
    }
    public void stopped()
    {
        anim.SetBool("isMoving", false);
       // Debug.Log("not isMoving");
    }
    public void isAiming(bool status)
    {
        anim.SetBool("isAiming", status);
    }
    public void Shoot()
    {
        anim.SetTrigger("Shoot"); 
    }
}
