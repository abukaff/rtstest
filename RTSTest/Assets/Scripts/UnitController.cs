﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MouseInput(int buttonNumber,Vector3 point)
    {
        switch (buttonNumber)
        {
            // Left Mouse Button
            case 0:
                if (gameObject.GetComponent<UnitData>().unitType == EnumsClass.UnitType.Dozer)
                {

                }

                break;
                //Right Mouse button
            case 1:
                //gameObject.GetComponent<NavAgentT90>().SetTargetPosition(point);
                MouseRightButton(point);
                break;
                //middle mouse button
            case 2:
                break;
            default:
                break;
        }

    }
    void KeyboardInput(KeyCode buttonKeycode)
    {

    }

    void MouseRightButton (Vector3 mouseLocation)
    {
        Ray ray = Camera.main.ScreenPointToRay(mouseLocation);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000))
        {

            if (hit.transform.name == "Terrain")
            {
                Vector3 targetLocation = hit.point;
                gameObject.GetComponent<UnitsNavigation>().SetDestination(targetLocation);
                gameObject.GetComponent<UnitAim>().cancelForceAim();
            }
            else
            {
                Vector3 targetLocation = hit.point;
                gameObject.GetComponent<UnitsNavigation>().SetDestination(targetLocation);
                gameObject.GetComponent<UnitAim>().ForceAimTarget(hit.transform.gameObject);
                
            }

        }


    }
}
