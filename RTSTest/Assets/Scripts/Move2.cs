﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move2 : MonoBehaviour {
    Vector3 tragetPosition;
    Vector3 lookAtTarget;
    Quaternion playerRot;
    public float rotSpeed = 5f;
    public float moveSpeed = 10f;
    bool moving = false;
    // Use this for initialization
    void Start () {
      //  tragetPosition = this.transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        bool RMB = Input.GetMouseButtonDown(1);
        if(RMB)
        {
            Debug.Log("Right CLicked");
            SetTargetPosition();
            
        }
        if(moving)
        Move();

    }

    void SetTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray,out hit,1000))
        {

            // hit.point.Normalize();

                tragetPosition = hit.point;
                Debug.Log(hit.point.ToString());

                lookAtTarget = new Vector3(
                    tragetPosition.x - this.transform.position.x,
                    this.transform.position.y,
                    tragetPosition.z - this.transform.position.z);
                playerRot = Quaternion.LookRotation(lookAtTarget);
                moving = true;


        }
    }
    void Move()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation,playerRot,rotSpeed * Time.deltaTime);
        tragetPosition.y = this.transform.position.y;
       /* transform.position = Vector3.Lerp(transform.position,
            tragetPosition,
            Time.deltaTime * moveSpeed);*/
        transform.position = Vector3.MoveTowards(transform.position,
            tragetPosition,
            Time.deltaTime * moveSpeed);

        if (Vector3.Distance(transform.position, tragetPosition) ==0)//transform.position == tragetPosition
            moving = false;
    }
}
