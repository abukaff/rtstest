﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD : MonoBehaviour {

    static List<Texture2D> unitsIconTexture = new List<Texture2D>();
    static List<Texture2D> unitsIconTextureRO = new List<Texture2D>();
    static List<string> unitsNames =  new List<string>();
    static List<string> unitPath = new List<string>();

    static List<Texture2D> objectsIconTexture = new List<Texture2D>();
    static List<Texture2D> objectsIconTextureRO = new List<Texture2D>();
    static List<string> objectsNames = new List<string>();
    static List<string> objectsPath = new List<string>();

    public Texture2D IconContainer;

    float HUDHeight;
    // MID BOX Dim
    float MidboxWidth;
    float MidboxHeight;
    float MidboxPosX; //Calculated at start
    float MidboxPosY;
    // Left BOX Dim
    float LeftboxWidth;
    float LeftboxHeight;
    float LeftboxPosX;
    float LeftboxPosY;
    // Right BOX Dim
    float RightboxWidth;
    float RightboxHeight;
    float RightboxPosX;
    float RightboxPosY;

    // Use this for initialization
    void Start () {

        MidboxWidth = Screen.width / 2;
        MidboxHeight = Screen.height / 4;
        MidboxPosX = MidboxWidth - MidboxWidth / 2;
        MidboxPosY = Screen.height - (Screen.height / 4);

        LeftboxWidth = Screen.width / 4;
        LeftboxHeight = Screen.height / 4;
        LeftboxPosX = 0;
        LeftboxPosY = (Screen.height / 4) * 3;

        RightboxWidth = Screen.width / 4;
        RightboxHeight = Screen.height / 4;
        RightboxPosX = (Screen.width / 4) * 3;
        RightboxPosY = (Screen.height / 4) * 3;


    }

	// Update is called once per frame
	void Update () {
        //HUDHeight = 

    }

    void OnGUI()
    {
        DrawHud();
        //Debug.Log("DRAW");
        if (unitsNames.Count > 0)
            UpdateUnitsGUI();
        else if (objectsNames.Count > 0)
            UpdateBuildingGUI();

    }
    void DrawHud()
    {
        GUIStyle Container = new GUIStyle();
        Container.normal.background = IconContainer;
        // GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height - 50, 400, 100), "", Container);
        // GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height - 50, 400, 100), "", Container);
        GUI.Box(new Rect(MidboxPosX, MidboxPosY, MidboxWidth, MidboxHeight), "", Container);
        GUI.Box(new Rect(LeftboxPosX, LeftboxPosY, LeftboxWidth, LeftboxHeight), "", Container);
        GUI.Box(new Rect(RightboxPosX, RightboxPosY, RightboxWidth, RightboxHeight), "", Container);
    }
    void UpdateUnitsGUI()
    {

        int sideoffset = 64;
        int topoffset = 50;
        for (int i = 0; i < unitsNames.Count; i++)
        {
            GUIStyle icon = new GUIStyle();
            icon.normal.background = unitsIconTexture[i];
            icon.hover.background = unitsIconTextureRO[i];
            //if (GUI.Button(new Rect(Screen.width / 2 - 199 + (i * sideoffset), Screen.height - 39, 46, 39), unitsNames[i], icon))
            if (GUI.Button(new Rect(MidboxPosX + (i * sideoffset), MidboxPosY, 46, 39), unitsNames[i], icon))
            {
                Debug.Log("Path Unit = " + unitPath[i]);
                CameraController.selectedBuilding.GetComponent<BuildingCommands>().BuildUnit(unitPath[i]);
            }
        }
    }

    void UpdateBuildingGUI()
    {
        
        int sideoffset = 64;
        int topoffset = 50;
        for (int i = 0; i < objectsNames.Count; i++)
        {
            GUIStyle icon = new GUIStyle();
            icon.normal.background = objectsIconTexture[i];
            icon.hover.background = objectsIconTextureRO[i];
            if (GUI.Button(new Rect(MidboxPosX + (i * sideoffset), MidboxPosY, 46, 39), objectsNames[i], icon))
            {
                Debug.Log("Path = " + objectsPath[i]);
                CameraController.selectionList[0].GetComponent<BuildingCommands>().BuildBuilding(objectsPath[i]);
            }
        }
    }


    public static void ClearUnits()
    {
        unitsIconTexture.Clear();
        unitsIconTextureRO.Clear();
        unitsNames.Clear();
        unitPath.Clear();

        objectsIconTexture.Clear();
        objectsIconTextureRO.Clear();
        objectsNames.Clear();
        objectsPath.Clear();

    }
    public static void AddToView(List<Texture2D> icons, List<Texture2D> iconsRO, List<string> names, List<string> path)
    {
        Debug.Log("AddToView");
        ClearUnits();
        for(int i=0;i< names.Count;i++)
        {
            unitsIconTexture.Add(icons[i]);
            unitsIconTextureRO.Add(iconsRO[i]);
            unitsNames.Add(names[i]);
            unitPath.Add(path[i]);
        }
    }
    public static void AddToView(List<BuildUnitClass> unitList)
    {
        Debug.Log("AddToView");
        ClearUnits();
        for (int i = 0; i < unitList.Count; i++)
        {
            unitsIconTexture.Add(unitList[i].unitsIconTexture);
            unitsIconTextureRO.Add(unitList[i].unitsIconTextureRO);
            unitsNames.Add(unitList[i].unitsName);
            unitPath.Add(unitList[i].unitPath);
        }
    }

    public static void AddToView(List<BuildClass> objectList)
    {
        Debug.Log("AddToView BuildClass");
        ClearUnits();
        for (int i = 0; i < objectList.Count; i++)
        {
            objectsIconTexture.Add(objectList[i].objectIconTexture);
            objectsIconTextureRO.Add(objectList[i].objectIconTextureRO);
            objectsNames.Add(objectList[i].objectName);
            objectsPath.Add(objectList[i].objectPath);
        }
    }
}
