﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAtTargetInRange : MonoBehaviour {

    public GameObject owner;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  

    void OnTriggerEnter(Collider collision)
    {
      /*  Debug.Log("COLL : " + collision.gameObject.name);
        owner.GetComponent<NavAgentT90>().AimAtTarget(collision.gameObject);*/
    }

    void OnTriggerStay(Collider collision)
    {
        try
        {
            /* if (collision.gameObject.GetComponent<PlayerData>().teamNumber != owner.GetComponent<PlayerData>().teamNumber)
                 owner.GetComponent<NavAgentT90>().AimAtTarget(collision.gameObject);*/
            owner.GetComponent<UnitAim>().AutoAimTarget(collision.gameObject);
        }
        catch
        {
            //Debug.LogError("AimAtTarget : " + collision.gameObject.name);
        }

    }

}
