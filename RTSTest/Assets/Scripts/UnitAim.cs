﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAim : MonoBehaviour {

    GameObject _autoAimTarget;
    bool _isAutoAimig = false;

    GameObject _forceAimTarget;
    bool _isForceAiming = false;

    [Tooltip("If no turret specified, this will cause the game object to rotate as whole")]
    public GameObject _turret;
    bool _hasTurret;

    [Tooltip("SphereCollider required for collision detection for autoaiming and fire destance")]
    public GameObject _rangeObject;
    float _range;

    public float aimRotationSpeed = 1.5f;

    Vector3 _targetPosition;

	// Use this for initialization
	void Start () {
        if (_turret != null)
            _hasTurret = true;
        _range = _rangeObject.GetComponent<SphereCollider>().radius;

    }
	
	// Update is called once per frame
	void Update () {
        AimAtTarget();

    }

    void AimAtTarget()
    {

        if(_isAutoAimig && !_isForceAiming)
        {
            float autoAimTargetdistance = Vector3.Distance(gameObject.transform.position, _autoAimTarget.transform.position);
            if (autoAimTargetdistance <= _range)
            {
                _targetPosition = _autoAimTarget.transform.position;
            }
            else
            {
                _autoAimTarget = null;
                _isAutoAimig = false;
            }
        }
        else if(_isForceAiming && _isAutoAimig)
        {
            float forcedAimTargetdistance = Vector3.Distance(gameObject.transform.position, _forceAimTarget.transform.position);
            float autoAimTargetdistance = Vector3.Distance(gameObject.transform.position, _autoAimTarget.transform.position);
            if (forcedAimTargetdistance <= _range)
            {
                _targetPosition = _forceAimTarget.transform.position;
                GetComponent<UnitsNavigation>().Stop();
            }else if (forcedAimTargetdistance > _range)
            {
                //if (GetComponent<UnitsNavigation>().isStopped())
                    GetComponent<UnitsNavigation>().SetDestination(_forceAimTarget.transform.position);
            }
            if(autoAimTargetdistance <= _range && forcedAimTargetdistance > _range)
            {
                _targetPosition = _autoAimTarget.transform.position;
            }else if (autoAimTargetdistance > _range)
            {
                _autoAimTarget = null;
                _isAutoAimig = false;
            }
            
        }
        else if (_isForceAiming)
        {
            float forcedAimTargetdistance = Vector3.Distance(gameObject.transform.position, _forceAimTarget.transform.position);
            if (forcedAimTargetdistance <= _range)
            {
                _targetPosition = _forceAimTarget.transform.position;
            }
            else if (forcedAimTargetdistance > _range)
            {
                //if (GetComponent<UnitsNavigation>().isStopped())
                    GetComponent<UnitsNavigation>().SetDestination(_forceAimTarget.transform.position);
            }
        }
        if(_isAutoAimig || _isForceAiming)
        RotateToTarget(_targetPosition);
    }

    void RotateToTarget(Vector3 targetPosition)
    {
        Vector3 lookAtTarget = new Vector3(
                 targetPosition.x - this.transform.position.x,
                 this.transform.position.y,
                 targetPosition.z - this.transform.position.z);
        Quaternion AimAT = Quaternion.LookRotation(lookAtTarget);
        if (_hasTurret)
            _turret.transform.rotation = Quaternion.Slerp(_turret.transform.rotation, AimAT, aimRotationSpeed * Time.deltaTime);
        else
        {
            if(gameObject.GetComponent<UnitsNavigation>().isStopped() == true)
            gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, AimAT, aimRotationSpeed * Time.deltaTime);
        }
    }

   /* void OnTriggerStay(Collider collision)
    {
        try
        {
            // if (collision.gameObject.GetComponent<PlayerData>().teamNumber != owner.GetComponent<PlayerData>().teamNumber)
            //     owner.GetComponent<NavAgentT90>().AimAtTarget(collision.gameObject);
            if (collision != null)
                _isAutoAimig = true;
            else
                _isAutoAimig = false;
        }
        catch
        {
            //Debug.LogError("AimAtTarget : " + collision.gameObject.name);
        }

    }*/

    public void AutoAimTarget(GameObject autoAimTarget)
    {
        float playerTeam = GetComponent<PlayerData>().teamNumber;
        float autoAimTargetTeam = autoAimTarget.GetComponent<PlayerData>().teamNumber;
        if (_isAutoAimig ==false && playerTeam != autoAimTargetTeam)
        {
            _autoAimTarget = autoAimTarget;
            _isAutoAimig = true;
        }
    }

    public void ForceAimTarget(GameObject forceAimTarget)
    {
        _forceAimTarget = forceAimTarget;
        _isForceAiming = true;
    }
    public void cancelForceAim()
    {
        _forceAimTarget = null;
        _isForceAiming = false;
    }
}
